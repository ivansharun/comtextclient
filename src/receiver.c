#include <stdio.h>
#include <unistd.h>
#include <termios.h>
#include "headers/rt.h"
#include "headers/cons.h"
#include <asm/ioctls.h>

extern int port;

void *receiveFunction(){
    char buffer[256];
    for(int i = 0; i < 256; i++){
		buffer[i] = 0;
	}
	int bytes;
    while(1){
		ioctl(port, FIONREAD, &bytes);
		if (bytes > 1){
			read(port, buffer, bytes);
			printMessage(buffer);
			for(int i = 0; i < 256; i++){
				buffer[i] = 0;
			}
		}
	}
}
