#include <stdio.h>
#include <pthread.h>
#include <fcntl.h>  
#include <string.h>
#include <locale.h>
#include <signal.h>
#include "headers/rt.h"
#include "headers/cons.h"

int port;
int EOS;
struct termios options;

int main(int argc, char *argv[]) {
	setlocale(LC_ALL, "");
	
	if (argc != 3){
		printf ("Incorrect arguments\n");
		printf ("Type: -[r|n] [port]\n");
		return 1;
	}
	
	if (!	strcmp(argv[1], "-r")){
		EOS = 1;
	} else {
		EOS = 0;
	}
	
	port = open(argv[2], O_RDWR | O_NOCTTY | O_NDELAY);
	if (port == -1) { 
		printf("Unable to open %s\n", argv[2]);
		return 1;
	}
	
	fcntl(port, F_SETFL, 0);
	
	tcgetattr(port, &options);
	
	options.c_cflag     |= (CLOCAL | CREAD);
	options.c_lflag     &= ~(ICANON | ECHO | ECHOE | ISIG);
	options.c_oflag     &= ~OPOST;
	options.c_cc[VMIN]  = 0;
	options.c_cc[VTIME] = 10;
	
	tcsetattr(port, TCSANOW, &options);

	pthread_t receiveThread;
	pthread_t consoleThread;
    
    if (pthread_create(&receiveThread, NULL, receiveFunction, NULL) != 0){
		printf ("receiveThread create error\n");
        return 2;
    }
    
    if (pthread_create(&consoleThread, NULL, consoleFunction, NULL) != 0){
		printf ("consoleThread create error\n");
        return 2;
    }
    
    pthread_join(consoleThread, NULL);    

	close(port);
	
	return 0;
}
