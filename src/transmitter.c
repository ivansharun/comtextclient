#include "headers/rt.h"

extern int port;
extern int EOS;

void sendMessage(char *buf){
	int length = 0;
	
	while(buf[length] != 0){
		length++;
	}
	if(EOS){
		buf[length - 1] = '\r';
	}
	write(port, buf, length);
}
