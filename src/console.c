#include <ncurses.h>
#include "headers/cons.h"
#include "headers/rt.h"

WINDOW *messagesWin;
WINDOW *subMessagesWin;

WINDOW *inputWin;
WINDOW *subInputWin;

void *consoleFunction(){
	initscr();
	curs_set(1);
	noecho();
	refresh();
	start_color();
	init_pair(1, COLOR_WHITE, COLOR_BLACK);
	init_pair(2, COLOR_BLACK, COLOR_GREEN);

	messagesWin = newwin(19, 80, 1, 0);
	subMessagesWin = subwin(messagesWin, 17, 76, 2, 2);
	scrollok(subMessagesWin, TRUE);

	inputWin = newwin(4, 80, 20, 0);
	subInputWin = newwin(2, 76, 21, 2);
	wattron(subInputWin, COLOR_PAIR(2));
	scrollok(subInputWin, TRUE);

	mvaddstr(0, 0, "Enter - send messge; F12 - exit;");
	mvaddstr(1, 2, "Messages:");
	mvaddstr(20, 2, "Input:");
	move(21, 2);

	box(messagesWin, 0, 0);
	box(inputWin, 0, 0);

	wrefresh(messagesWin);
	wrefresh(inputWin);

	int i = 0;
	char buf[255];
	char inputChar;
	do{
		inputChar = getch(); 
		buf[i] = inputChar;
		i++;
		if (inputChar == '~') break;
		switch(inputChar){
			case 10:
				if(buf[0] != 10){
					wattron(subMessagesWin, COLOR_PAIR(2));
					printMessage(buf);
					wattron(subMessagesWin, COLOR_PAIR(1));
					sendMessage(buf);
					wclear(subInputWin);
					wrefresh(subInputWin);
					for(int j = 0; j < 256; j++) buf[j] = 0; 
				}
				i = 0;
				break;
			default:
				waddch(subInputWin,inputChar);
				wrefresh(subInputWin);
				break;
			}
	} while(inputChar !=  '~');

	delwin(subMessagesWin);
	delwin(messagesWin);
	delwin(subInputWin);
	delwin(inputWin);
	endwin();
}

void printMessage(char *buf){
	waddstr(subMessagesWin, buf);
	wrefresh(subMessagesWin);
	wrefresh(subInputWin);
}
