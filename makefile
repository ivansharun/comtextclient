bin/ComTextClient: bin/main.o bin/receiver.o bin/transmitter.o bin/console.o
	gcc -lncursesw -lpthread -o bin/ComTextClient bin/main.o bin/receiver.o bin/transmitter.o bin/console.o

bin/main.o: src/headers/rt.h src/main.c 
	gcc -c -o bin/main.o src/main.c 

bin/receiver.o: src/headers/rt.h src/receiver.c
	gcc -std=c99 -c -o bin/receiver.o src/receiver.c 

bin/transmitter.o: src/headers/rt.h src/transmitter.c
	gcc -std=c99 -c -o bin/transmitter.o src/transmitter.c 

bin/console.o: src/console.c src/headers/cons.h
	gcc -std=c99 -c -o bin/console.o src/console.c
	
clean:
	rm bin/*

